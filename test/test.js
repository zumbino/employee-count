
describe('people-names', function () {

  var assert = require('assert')
    , should = require('should')
    , count = require('..');

  describe('.get()', function () {
    it('should loose compare', function () {
      var testString = '500-900 employees';
      var value = count.get(testString);
      console.log(value);
      value.value.should.be.within(500, 900);
      value.errorRange.should.be.below(200);

      testString = '1-10 employees';
      value = count.get(testString);
      console.log(value);
      value.value.should.be.within(1, 10);
      value.errorRange.should.be.below(4.5);

      testString = '500+ employees';
      value = count.get(testString);
      console.log(value);
      value.value.should.be.within(500, 550);
      value.errorRange.should.be.below(25);

      testString = '\nMyself Only ';
      value = count.get(testString);
      console.log(value);
      value.value.should.be.equal(1);
      value.errorRange.should.be.equal(0);
    });
  });

});
