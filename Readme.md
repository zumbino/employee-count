
# employee-count

  Work with fuzzy strings to give estimates of numerical values contained

## Installation

    $ npm install employee-count

## API

### .get(someString)

  Returns an object with structure:
  {
    value: number,
    errorRange: number
  }


## License

  MIT
