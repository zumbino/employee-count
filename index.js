
var MATCH_REGEX = /(\d+)(\+)?([^0-9-]*)?(-)?([^0-9]*)?(\d*)?(\+)?/;

module.exports.get = function(inputString) {
  if (!inputString) return null;
  if (inputString.trim() === 'Myself Only') return {value: 1, errorRange: 0};
  var match = inputString.match(MATCH_REGEX);
  if (!match) return null;
  var lowerBound = parseInt(match[1], 10);
  var lowerPlus = !!match[2];
  // match[3] is garbage
  var isRange = !!match[4];
  // match[5] is garbage
  var upperBound = match[6];
  var upperPlus = !!match[7];

  // logic is lower bound plus a random number < (lowerBound - upperBound)
  // if no upper bound create one
  if (!upperBound) {
    if (lowerPlus) {
      upperBound = 1.1 * lowerBound;
    } else {
      upperBound = lowerBound;
    }
  } else {
    upperBound = parseInt(upperBound, 10);
  }

  var estimate = Math.floor(lowerBound + (Math.random() * (upperBound - lowerBound)));
  var error = Math.min(estimate - lowerBound, upperBound - estimate);

  return {
    value: estimate,
    errorRange: error
  };
};
